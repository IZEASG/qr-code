// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ["@nuxt/image",
      "@nuxtjs/google-fonts"
  ],

  googleFonts: {
    families: {
      Outfit: [400, 700],
    }
  },
})
